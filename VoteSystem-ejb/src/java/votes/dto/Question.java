/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Question extends AbstractDTO{
    
    private String mode;
    private String title;
    private int maxAnswers;
    private int abstentions;
    
    public Question(){
        
    }

    public Question(String uuid, long jpaVersion, String mode, String title, int maxAnswers, int abstentions) {
        this.mode = mode;
        this.title = title;
        this.maxAnswers = maxAnswers;
        this.abstentions = abstentions;
    }
    
    public Question(String uuid, long jpaVersion, String mode, String title, int maxAnswers) {
        this.mode = mode;
        this.title = title;
        this.maxAnswers = maxAnswers;
        this.abstentions = 0;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxAnswers() {
        return maxAnswers;
    }

    public void setMaxAnswers(int maxAnswers) {
        this.maxAnswers = maxAnswers;
    }

    public int getAbstentions() {
        return abstentions;
    }

    public void setAbstentions(int abstentions) {
        this.abstentions = abstentions;
    }
    
}
