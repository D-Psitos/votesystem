/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Item extends AbstractDTO{
    
    private String name;
    private int votes;
    
    public Item(){
        
    }
    
    public Item(String uuid, long jpaVersion, String name, int votes){
        super(uuid, jpaVersion);
        this.name = name;
        this.votes = votes;        
    }
    
    public Item(String uuid, long jpaVersion, String name){
        super(uuid, jpaVersion);
        this.name = name;
        this.votes = 0;        
    }
        
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }
    
}
