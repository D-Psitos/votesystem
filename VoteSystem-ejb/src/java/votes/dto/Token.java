/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Token extends AbstractDTO {
    
    private String tokenValue;
    
    public Token() {
        
    }

    public Token( String uuid, long jpaVersion, String tokenValue) {
        super(uuid, jpaVersion);
        this.tokenValue = tokenValue;
    }
    
    public Token(String uuid, long jpaVersion) {
        super(uuid, jpaVersion);
        this.tokenValue = "TODO";
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }
    
}
