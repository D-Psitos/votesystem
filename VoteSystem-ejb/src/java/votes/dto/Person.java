package votes.dto;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dimitris
 */
@XmlRootElement
public class Person extends AbstractDTO {

    private String email;
    private String firstName;
    private String lastName;

    public Person() {
    }

    public Person(String uuid, long jpaVersion, String email, String firstName, String lastName) {
        super(uuid, jpaVersion);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

