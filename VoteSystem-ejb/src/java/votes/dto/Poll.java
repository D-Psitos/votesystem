/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.dto;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;
import votes.logic.impl.PollState;

@XmlRootElement
public class Poll extends AbstractDTO{
    
    private String pollState;
    private Date startDate;
    private Date endDate;
    private int submittedVotes;
    
    public Poll(){
        
    }

    public Poll(String uuid, long jpaVersion, String state, Date startDate, Date endDate, int submittedVotes) {
        super(uuid, jpaVersion);
        this.pollState = state;
        this.startDate = startDate;
        this.endDate = endDate;
        this.submittedVotes = submittedVotes;
    }
    
    public Poll(String uuid, long jpaVersion, String state, Date startDate, Date endDate) {
        super(uuid, jpaVersion);
        this.pollState = PollState.PREPARED;
        this.startDate = startDate;
        this.endDate = endDate;
        this.submittedVotes = 0;
    }

    public String getState() {
        return pollState;
    }

    public void setState(String state) {
        this.pollState = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getSubmittedVotes() {
        return submittedVotes;
    }

    public void setSubmittedVotes(int submittedVotes) {
        this.submittedVotes = submittedVotes;
    } 
    
}
