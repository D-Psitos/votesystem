package votes.dao;

import votes.entities.PersonEntity;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * @author Dr. Volker Riediger <riediger@uni-koblenz.de>
 */
@LocalBean
@Stateless
public class PersonAccess {

    @PersistenceContext(unitName = "Contacts-ejbPU") // TO CHECK LATER
    private EntityManager em;

    public PersonEntity createPerson(String email, String firstName, String lastName) {
        PersonEntity p = new PersonEntity();
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setEmail(email);
        em.persist(p);
        return p;
    }

    public List<List<PersonEntity>> getPersonList() {
        return em.createNamedQuery("getPersonList").getResultList();
    }
/*    
    public List<PollEntity> getOwnedPolls() {
        return em.createNamedQuery("getOwnedPolls").getResultList();
    }
    
    public List<PollEntity> getParticipatedPolls() {
        return em.createNamedQuery("getParticipatedPolls").getResultList();
    }
*/    
    public PersonEntity getPersonByUuid(String uuid) {
        try {
            return em.createNamedQuery("getPersonByUuid", PersonEntity.class).
                    setParameter("uuid", uuid)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public boolean deleteContact(String uuid) {
        PersonEntity p = getPersonByUuid(uuid);
        if (p == null) {
            return false;
        }
        em.remove(p);
        return true;
    }
}
