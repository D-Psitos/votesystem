/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import votes.dto.Poll;

@Entity
public class PollEntity extends AbstractEntity implements Serializable {
    
    private String pollState;
    
    @Temporal(TemporalType.TIME)
    private Date startDate;
    
    @Temporal(TemporalType.TIME)
    private Date endDate;
    
    private Integer submittedVotes;
    
    @OneToMany(mappedBy="poll")
    private List<QuestionEntity> questions;
    
    @OneToMany(mappedBy="poll")
    private Set<TokenEntity> tokens;
    
    @ManyToOne
    private PersonEntity organizer;
            
    public String getState() {
        return pollState;
    }

    public void setState(String state) {
        this.pollState = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getSubmittedVotes() {
        return submittedVotes;
    }

    public void setSubmittedVotes(Integer submittedVotes) {
        this.submittedVotes = submittedVotes;
    } 

    public List<QuestionEntity> getQuestions() {
        return questions;
    }
    
    public Set<TokenEntity> getTokens() {
        return tokens;
    }

    public PersonEntity getOrganizer() {
        return organizer;
    }

    public void setOrganizer(PersonEntity organizer) {
        this.organizer = organizer;
    }
    
    public Poll createDTO() {
        return new Poll(getUuid(), getJpaVersion(), pollState, startDate, endDate, submittedVotes);
    }
}
