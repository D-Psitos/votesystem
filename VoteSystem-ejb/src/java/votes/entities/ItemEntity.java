/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.entities;

import votes.dto.Item;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;


@Entity
public class ItemEntity extends AbstractEntity implements Serializable{
    
    private String name;
    private Integer votes;
    
    @ManyToOne
    private QuestionEntity question;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public QuestionEntity getQuestion() {
        return question;
    }

    public void setQuestion(QuestionEntity question) {
        this.question = question;
    }
    
    public Item createDTO() {
        return new Item(getUuid(), getJpaVersion(), name, votes);
    }
}
