/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import votes.dto.Question;

@Entity
public class QuestionEntity extends AbstractEntity implements Serializable{
    
    private String mode;
    private String title;
    private Integer maxAnswers;
    private Integer abstentions;
    
    @OneToMany(mappedBy="question")
    private List<ItemEntity> items;
    
    @ManyToOne
    private PollEntity poll;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMaxAnswers() {
        return maxAnswers;
    }

    public void setMaxAnswers(Integer maxAnswers) {
        this.maxAnswers = maxAnswers;
    }

    public int getAbstentions() {
        return abstentions;
    }

    public void setAbstentions(Integer abstentions) {
        this.abstentions = abstentions;
    }

    public List<ItemEntity> getItems() {
        return items;
    }

    public PollEntity getPoll() {
        return poll;
    }

    public void setPoll(PollEntity poll) {
        this.poll = poll;
    }
    
    public Question createDTO() {
        return new Question(getUuid(), getJpaVersion(), mode,  title, maxAnswers, abstentions);
    }
    
    
}
