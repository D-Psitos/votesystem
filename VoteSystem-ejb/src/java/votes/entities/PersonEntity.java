/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.entities;

import votes.dto.Person;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Dimitris
 */
@NamedQueries({
    @NamedQuery(
            name = "getPersonList",
            query = "SELECT p FROM PersonEntity p ORDER BY p.lastName"
    )
    ,
    @NamedQuery(
            name = "getPersonByUuid",
            query = "SELECT p FROM PersonEntity p WHERE p.uuid = :uuid"
    )
    ,
    @NamedQuery(
            name = "deletePersonByUuid",
            query = "DELETE FROM ContactEntity p WHERE p.uuid = :uuid"
    )
})
@Entity
public class PersonEntity extends AbstractEntity implements Serializable{

    private String email;
    private String firstName;
    private String lastName;
    
    @OneToMany
    private Set<Set<PersonEntity>> participantLists; //the lists that a person owns
    @OneToMany
    private Set<PollEntity> ownedPolls;
    @OneToMany
    private Set<PollEntity> participatedPolls;

    public Set<Set<PersonEntity>> getParticipantLists() {
        return participantLists;
    }
    
    public void addParticipantList(Set<PersonEntity> newList){
        participantLists.add(newList);
    }
    
    public Set<PollEntity> getOwnedPolls() {
        return ownedPolls;
    }
    
    public void addOwnedPoll(PollEntity newPoll){
        ownedPolls.add(newPoll);
    }
    
    public Set<PollEntity> getParticipatedPolls() {
        return participatedPolls;
    }
    
    public void addParticipatedPoll(PollEntity newPoll){
        participatedPolls.add(newPoll);
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.lastName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Person createDTO() {
        return new Person(getUuid(), getJpaVersion(), email, firstName, lastName);
    }
}
