/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import votes.dto.Token;

@Entity
public class TokenEntity extends AbstractEntity implements Serializable {
    
    private String tokenValue;
    
    @ManyToOne
    private PollEntity poll;
    
    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public PollEntity getPoll() {
        return poll;
    }
    
    public Token createDTO() {
        return new Token(getUuid(), getJpaVersion(), tokenValue);
    }
}
