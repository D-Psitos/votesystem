package votes.logic.impl;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public class DecisionMode {
    
    public static final String ABSOLUTE_MAJORITY = "ABSOLUTE_MAJORITY";
    public static final String RELATIVE_MAJORITY = "RELATIVE_MAJORITY";
    public static final String SIMPLE_MAJORITY = "SIMPLE_MAJORITY";
    
}
