/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package votes.logic.impl;

public class PollState {
    
    public static final String PREPARED = "PREPARED";
    public static final String STARTED = "STARTED";
    public static final String VOTING = "VOTING";
    public static final String FINISHED = "FINISHED";
    
}
